package com.teclo.kioskoteclo.layouts;

import android.appwidget.AppWidgetHostView;

/**
 * Created by Omar Tovias CreepyAnimations on 16/06/2018.
 */

public class WidgetInfo {

    public AppWidgetHostView hostView;
    private final int appWidgetId;

    public WidgetInfo(int appWidgetId) {
        this.appWidgetId = appWidgetId;
    }

}
