package com.teclo.kioskoteclo.utils;

import android.app.Activity;
import android.util.DisplayMetrics;

/**
 * Created by Omar Tovias CreepyAnimations on 27/06/2018.
 */

public class SoftButtonsSizeRetriever {

    public static int getSoftButtonsBarHeight(Activity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int usableHeight = metrics.heightPixels;
        activity.getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
        int realHeight = metrics.heightPixels;
        if (realHeight > usableHeight)
            return realHeight - usableHeight;
        else
            return 0;
    }
}
