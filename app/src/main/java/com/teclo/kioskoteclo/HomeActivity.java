package com.teclo.kioskoteclo;

import android.app.ActionBar;
import android.app.Activity;
import android.appwidget.AppWidgetHost;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProviderInfo;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.ContentObserver;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CallLog;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.teclo.kioskoteclo.layouts.CustomViewGroup;
import com.teclo.kioskoteclo.layouts.WidgetFrame;
import com.teclo.kioskoteclo.layouts.WidgetInfo;
import com.teclo.kioskoteclo.utils.AppsSorter;
import com.teclo.kioskoteclo.utils.IconLoader;
import com.teclo.kioskoteclo.utils.MissedCallsCountRetriever;
import com.teclo.kioskoteclo.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.teclo.kioskoteclo.utils.SharedPrefs.getCurrentWidgetPage;
import static com.teclo.kioskoteclo.utils.SoftButtonsSizeRetriever.getSoftButtonsBarHeight;

/**
 * Last Edited by Omar Tovias CreepyAnimations on 28/06/2018.
 */

public class HomeActivity extends Activity {


    private GestureDetectorCompat mGestureDetector;
    private PackageManager mPackageManager;
    private Context mContext;
    private LinearLayout mDockLayout;
    private RelativeLayout mTopContainer;
    private boolean mTopContainerEnabled = true;
    private boolean mIsWidgetPinned;
    private int mTopContainerWidth;
    private int mTopContainerHeight;
    private ScrollView mWidgetScrollView;
    private LinearLayout mWidgetContainer;
    private int mSingleScrollHeight;

    private int mCurrentWidgetPage;
    private boolean mAllScrollsDisabled = false;
    private android.app.AlertDialog alert;

    ///////////
    private AppWidgetManager mAppWidgetManager;
    private static final int REQUEST_CREATE_APPWIDGET = 5;
    private static final int REQUEST_PICK_APPWIDGET   = 9;
    private static final int TIME_TO_LIGHT = 1500;
    ///////////

    private BroadcastReceiver mPackageUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            reloadCarousel();
        }
    };

    private BroadcastReceiver mSmsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            reloadCarousel();
        }
    };


    private ContentObserver missedCallObserver = new ContentObserver(null) {
        @Override
        public void onChange(boolean selfChange) {
            reloadCarousel();
        }
    };
    private WidgetFrame newWidgetPage;
    private ArrayList<Integer> widgetIds = new ArrayList<>();
    private int startScrollX;
    private int endScrollX;
    private RelativeLayout.LayoutParams mResizeRightLayoutParams;
    private RelativeLayout.LayoutParams mResizeDownLayoutParams;
    private String mResizeButtonTag;
    private int mResizeStartX;
    private int mResizeStartY;
    private int mCurrentWidgetMinHeight;
    private int mCurrentWidgetMinWidth;
    private Button mResizeDown;
    private Button mResizeRight;
    private int mCurrentResizeDownLeftMargin;
    private int mCurrentResizeDownTopMargin;
    private int mCurrentResizeRightLeftMargin;
    private int mCurrentResizeRightTopMargin;
    private WidgetInfo launcherInfo;
    private int mCurrentWidgetWidth;
    private int mCurrentWidgetHeight;
    private FloatingActionButton mGoToSettings;
    private ArrayList<FloatingActionButton> mControls = new ArrayList<>();
    private AlertDialog.Builder mBuilder;
    private AlertDialog mDialog;
    private CustomViewGroup mView;
    private WindowManager mManager;
    private FloatingActionButton mRefreshWidget;

    private boolean mPinEntered;
    private String mPassPhrase;

    private void reloadCarousel() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mDockLayout.removeAllViews();
                loadCarousel();
            }
        }, 200);
    }

    @Override
    protected void onStart() {
        mSmsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                reloadCarousel();
            }
        };
        registerReceiver(mSmsReceiver, new IntentFilter(
                "android.provider.Telephony.SMS_RECEIVED"));
        getApplicationContext().getContentResolver().registerContentObserver(CallLog.Calls.CONTENT_URI, true, missedCallObserver);

        mPackageUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                reloadCarousel();
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
        intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        registerReceiver(mPackageUpdateReceiver, intentFilter);

        int unreadMessages = MissedCallsCountRetriever.getUnreadMessagesCount(mContext);
        if(mDockLayout != null) {
            for(int i = 0; i < mDockLayout.getChildCount(); i++) {
                if(mDockLayout.getChildAt(i).getTag().toString().equals("Messaging")) {
                    int unreadMsgCount = 0;
                    String unreadMessagesCount = ((TextView)((FrameLayout)((LinearLayout)(mDockLayout.getChildAt(i))).getChildAt(0)).getChildAt(1)).getText().toString();
                    try {
                        unreadMsgCount = Integer.parseInt(unreadMessagesCount);
                    } catch (Exception ignored) {

                    }
                    if( unreadMsgCount != unreadMessages) {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mDockLayout.removeAllViews();
                                loadCarousel();
                            }
                        }, 50);
                    }
                }
            }
        }
        if( mWidgetContainer.getChildCount() > 0) {
            mCurrentWidgetPage = SharedPrefs.getCurrentWidgetPage(mContext);
            mWidgetScrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    mSingleScrollHeight = mWidgetScrollView.getHeight();
                    int screenWidth = mWidgetScrollView.getWidth();
                    SharedPrefs.saveScreenWidth(mContext, screenWidth);
                    mWidgetScrollView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    mWidgetScrollView.postDelayed(new Runnable() {
                        public void run() {
                            mWidgetScrollView.smoothScrollTo(0, mCurrentWidgetPage * mSingleScrollHeight);
                        }
                    },10);
                    ((WidgetFrame) mWidgetContainer.getChildAt(mCurrentWidgetPage)).getAppWidgetHost().startListening();
                    //mWidgetScrollView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });

        }
        if(SharedPrefs.getSafeModeOn(mContext)) {
            mManager = ((WindowManager) getApplicationContext()
                    .getSystemService(Context.WINDOW_SERVICE));

            WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
            localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
            localLayoutParams.gravity = Gravity.TOP;
            localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

            localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            localLayoutParams.height = (int) (50 * getResources()
                    .getDisplayMetrics().scaledDensity);
            localLayoutParams.format = PixelFormat.TRANSPARENT;

            mView = new CustomViewGroup(this);
            mView.setId(999999);

            mManager.addView(mView, localLayoutParams);
        } else {
            if(mView != null) {
                mManager.removeViewImmediate(mView);
            }
        }
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        SharedPrefs.setPasshrase(this,Constants.PASS);
        SharedPrefs.setSafeModeOn(true, this);

        mContext = this;

        mAppWidgetManager = AppWidgetManager.getInstance(this);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.TRANSPARENT);
        window.setNavigationBarColor(Color.TRANSPARENT);

        mIsWidgetPinned = SharedPrefs.getWidgetPinned(mContext);

        /////////////// load/ process icons //////////////
        //startIntentService();

        // comment out if loading icons from local storage
        loadCarousel();

        mWidgetScrollView = (ScrollView) findViewById(R.id.widgets_scroll);


        mWidgetContainer = (LinearLayout) findViewById (R.id.widget_container);

        mGoToSettings = (FloatingActionButton) findViewById(R.id.go_to_settings);
        mGoToSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDialog();

            }
        });


        mRefreshWidget = (FloatingActionButton) findViewById(R.id.refresh_widget);
        mRefreshWidget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mWidgetContainer.getChildCount() > 0) {
                    mWidgetContainer.removeAllViews();
                    SharedPrefs.setCurrentWidgetPage(mContext, mCurrentWidgetPage);
                    loadSavedWidgets(widgetIds);
                } else {
                    Toast.makeText(HomeActivity.this,"No widget to refresh" ,
                            Toast.LENGTH_SHORT).show();
                }
            }
        });



        mControls.add(mGoToSettings);

        mGestureDetector = new GestureDetectorCompat(this, new MyGestureListener());
        RelativeLayout home = (RelativeLayout) findViewById(R.id.home_container);
        home.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mGestureDetector.onTouchEvent(event);
                return false;
            }
        });

        home.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
            if(mTopContainerEnabled) {
                mTopContainer.setVisibility(View.INVISIBLE);
                mTopContainer.setEnabled(false);
            } else {
                mTopContainer.setVisibility(View.VISIBLE);
                mTopContainer.setEnabled(true);
            }
            mTopContainerEnabled =!mTopContainerEnabled;
            return false;
            }
        });
        ArrayList<Integer> widgetDetails = SharedPrefs.getWidgetsIds(mContext);
        if(mWidgetContainer.getChildCount() != widgetDetails.size()) {
            loadSavedWidgets(widgetDetails);
        }


        Handler handler = new Handler();

        handler.postDelayed(() -> {
            loadDefaultApp();

        }, TIME_TO_LIGHT);


    }

    public void showSimpleAlert(String message, Context context){

        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(true)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        alert = builder.create();
        alert.show();
    }

    private void loadDefaultApp(){
       // openApp(this,Constants.DEFAULT_APP_NAME, Constants.DEFAULT_APP_PACKAGE);
        PackageManager manager = mContext.getPackageManager();

        Intent launchIntent = manager.getLaunchIntentForPackage(Constants.DEFAULT_APP_PACKAGE);
        if (launchIntent != null) {
            startActivity(launchIntent);//null pointer check in case package name was not found
        }else{
            showSimpleAlert("App no instalada", mContext);
        }

        //getAppLogosArray();
    }

    private void toggleNavigationBar(){
        View decorView = getWindow().getDecorView();
        int uiOptions =
                 View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;

        decorView.setSystemUiVisibility(uiOptions);


    }

    private void toggleNotifications(){

    }

    public ArrayList<Drawable> getAppLogosArray() {
        PackageManager pm = mContext.getPackageManager();
        List<ApplicationInfo> listOfAppInfo = pm.getInstalledApplications(pm.GET_META_DATA);

        ArrayList<Drawable>  appLogosArray = new ArrayList<>();

        for(ApplicationInfo app : listOfAppInfo) {

            if(pm.getLaunchIntentForPackage(app.packageName) != null) {
                // apps with launcher intent
                if((app.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) == 1) {
                    // updated system apps

                } else if ((app.flags & ApplicationInfo.FLAG_SYSTEM) == 1) {
                    // system apps

                } else {
                    // user installed apps
                    appLogosArray.add(app.loadIcon(pm));

                }
            }

        }
        return appLogosArray;
    }

    private void goToSettings() {
        Intent i = new Intent(mContext, SettingsActivity.class);
        startActivity(i);
    }

    private void showDialog() {
        mBuilder = new AlertDialog.Builder(this);
        mBuilder.setTitle("Ingresa la contraseña");
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.pin_input, null, false);
        final EditText pin_input = (EditText) viewInflated.findViewById(R.id.pin);
        mBuilder.setView(viewInflated);

        mBuilder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        mBuilder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        mDialog = mBuilder.create();
        mDialog.show();
        mDialog.setCancelable(false);
        mDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                    if(pin_input.getText().toString().equals(SharedPrefs.getPassphrase(mContext))) {
                        mDialog.dismiss();

                        goToSettings();

                    } else {
                        pin_input.setText("");
                        mDialog.setTitle("Intenta de nuevo");
                    }
                }
        });
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_PICK_APPWIDGET:
                    addAppWidget(data);
                    break;
                case REQUEST_CREATE_APPWIDGET:
                    completeAddAppWidget(data);
                    break;
            }
        }
    }


    private void loadSavedWidgets(ArrayList<Integer> ids) {
        widgetIds = ids;
        for(int i = 0; i < ids.size(); i++) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            newWidgetPage = (WidgetFrame) inflater.inflate(R.layout.widget_layout, mWidgetContainer, false);
            newWidgetPage.setTag(new Random().nextInt(Integer.MAX_VALUE));
            newWidgetPage.setAppWidgetHost(new AppWidgetHost(mContext, Integer.parseInt(newWidgetPage.getTag().toString())));

            int appWidgetId = ids.get(i);
            AppWidgetProviderInfo appWidgetInfo = mAppWidgetManager.getAppWidgetInfo(appWidgetId);

            WidgetInfo launcherInfo = new WidgetInfo(appWidgetId);
            launcherInfo.hostView = newWidgetPage.getAppWidgetHost().createView(this, appWidgetId, appWidgetInfo);
            launcherInfo.hostView.setAppWidget(appWidgetId, appWidgetInfo);
            launcherInfo.hostView.setTag(launcherInfo);
            mCurrentWidgetWidth = SharedPrefs.getWidgetWidth(mContext, appWidgetId);
            mCurrentWidgetHeight = SharedPrefs.getWidgetHeight(mContext, appWidgetId);
            launcherInfo.hostView.setLayoutParams(new FrameLayout.LayoutParams(mCurrentWidgetWidth,mCurrentWidgetHeight));
            FrameLayout.LayoutParams widgetParams = (FrameLayout.LayoutParams) launcherInfo.hostView.getLayoutParams();
            widgetParams.setMargins((mTopContainerWidth - mCurrentWidgetWidth) / 2, (mTopContainerHeight - mCurrentWidgetHeight) / 2, (mTopContainerWidth - mCurrentWidgetWidth) / 2, (mTopContainerHeight - mCurrentWidgetHeight) / 2);
            launcherInfo.hostView.setLayoutParams(widgetParams);
            newWidgetPage.setWidgetView(launcherInfo);

            mWidgetContainer.addView(newWidgetPage);
        }

    }

    private void completeAddAppWidget(Intent data) {
        Bundle extras = data.getExtras();
        final int appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);

        AppWidgetProviderInfo appWidgetInfo = mAppWidgetManager.getAppWidgetInfo(appWidgetId);

        launcherInfo = new WidgetInfo(appWidgetId);
        launcherInfo.hostView = newWidgetPage.getAppWidgetHost().createView(this, appWidgetId, appWidgetInfo);
        launcherInfo.hostView.setAppWidget(appWidgetId, appWidgetInfo);
        launcherInfo.hostView.setTag(launcherInfo);
        launcherInfo.hostView.setLayoutParams(new FrameLayout.LayoutParams(appWidgetInfo.minWidth, appWidgetInfo.minHeight));

        newWidgetPage.setWidgetView(launcherInfo);
        widgetIds.add(appWidgetId);
        SharedPrefs.saveWidgetsIds(mContext, widgetIds);
        mWidgetContainer.addView(newWidgetPage);
        mWidgetScrollView.post(new Runnable() {
            public void run() {
                mWidgetScrollView.smoothScrollTo(0, (mWidgetContainer.getChildCount() -1) * mSingleScrollHeight);
            }
        });

        mCurrentWidgetPage = mWidgetContainer.getChildCount() - 1;
        SharedPrefs.setCurrentWidgetPage(mContext,mCurrentWidgetPage);

        SharedPrefs.setCurrentWidgetPage(mContext, mCurrentWidgetPage);
        newWidgetPage.getAppWidgetHost().startListening();

        mResizeDown = (Button) findViewById(R.id.resize_widget_down);
        mResizeDown.setVisibility(View.VISIBLE);

        mResizeRight = (Button) findViewById(R.id.resize_widget_right);
        mResizeRight.setVisibility(View.VISIBLE);

        mCurrentWidgetMinHeight = appWidgetInfo.minHeight;
        mCurrentWidgetMinWidth = appWidgetInfo.minWidth;
        mCurrentWidgetHeight = appWidgetInfo.minHeight;
        mCurrentWidgetWidth = appWidgetInfo.minWidth;

        mResizeDownLayoutParams = new RelativeLayout.LayoutParams(mResizeDown.getLayoutParams());
        mResizeDownLayoutParams.height = 60;
        mResizeDownLayoutParams.width = 60;
        mCurrentResizeDownTopMargin = mCurrentWidgetMinHeight - 30;
        mResizeDownLayoutParams.topMargin = mCurrentResizeDownTopMargin;
        mCurrentResizeDownLeftMargin = mCurrentWidgetMinWidth / 2 - 30;
        mResizeDownLayoutParams.leftMargin = mCurrentResizeDownLeftMargin;
        mResizeDown.setLayoutParams(mResizeDownLayoutParams);
        mResizeDown.setOnTouchListener(new MyTouchListener());

        mResizeRightLayoutParams = new RelativeLayout.LayoutParams(mResizeRight.getLayoutParams());
        mResizeRightLayoutParams.height = 60;
        mResizeRightLayoutParams.width = 60;
        mCurrentResizeRightLeftMargin = mCurrentWidgetMinWidth - 30;
        mCurrentResizeRightTopMargin = mCurrentWidgetMinHeight / 2 - 30;
        mResizeRightLayoutParams.topMargin = mCurrentResizeRightTopMargin;
        mResizeRightLayoutParams.leftMargin = mCurrentResizeRightLeftMargin;
        mResizeRight.setLayoutParams(mResizeRightLayoutParams);
        mResizeRight.setOnTouchListener(new MyTouchListener());

        mGoToSettings.setVisibility(View.GONE);
        mRefreshWidget.setVisibility(View.GONE);
        mAllScrollsDisabled = true;
        final FloatingActionButton addWidget = (FloatingActionButton) findViewById(R.id.complete_add_widget);
        if(mWidgetContainer.getChildCount() > 0) {
            FrameLayout.LayoutParams widgetParams = (FrameLayout.LayoutParams) launcherInfo.hostView.getLayoutParams();
            widgetParams.setMargins(0, 0, 0, mTopContainerHeight - mCurrentWidgetMinHeight);
            launcherInfo.hostView.setLayoutParams(widgetParams);
            mWidgetScrollView.post(new Runnable() {
                public void run() {
                    mWidgetScrollView.smoothScrollTo(0, (mWidgetContainer.getChildCount() -1)* mTopContainerHeight);
                }
            });
        }
        addWidget.setVisibility(View.VISIBLE);
        addWidget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FrameLayout.LayoutParams widgetParams = (FrameLayout.LayoutParams) launcherInfo.hostView.getLayoutParams();
                if(mCurrentWidgetHeight % 2 == 1) {
                    mCurrentWidgetHeight++;
                }
                if(mCurrentWidgetWidth % 2 == 1) {
                    mCurrentWidgetWidth++;
                }
                widgetParams.setMargins((mTopContainerWidth - mCurrentWidgetWidth) / 2, (mTopContainerHeight - mCurrentWidgetHeight) / 2, (mTopContainerWidth - mCurrentWidgetWidth) / 2, (mTopContainerHeight - mCurrentWidgetHeight) / 2);
                launcherInfo.hostView.setLayoutParams(widgetParams);
                addWidget.setVisibility(View.GONE);
                mResizeDown.setVisibility(View.GONE);
                mResizeRight.setVisibility(View.GONE);
                SharedPrefs.setWidgetHeight(mContext, mCurrentWidgetHeight, appWidgetId);
                SharedPrefs.setWidgetWidth(mContext, mCurrentWidgetWidth, appWidgetId);
                mAllScrollsDisabled = false;
                if(mIsWidgetPinned) {
                    SharedPrefs.setCurrentWidgetPage(mContext, mCurrentWidgetPage);
                }
            }
        });
    }

    private final class MyTouchListener implements View.OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {

            switch (motionEvent.getAction()) {

                case MotionEvent.ACTION_DOWN:
                    mResizeButtonTag = view.getTag().toString();
                    mResizeStartX = (int) (view.getX() - motionEvent.getRawX());
                    mResizeStartY = (int) (view.getY() - motionEvent.getRawY());
                    break;

                case MotionEvent.ACTION_MOVE:
                    switch (mResizeButtonTag) {
                        case "resize_down":
                            if (motionEvent.getRawY() + mResizeStartY > mCurrentWidgetMinHeight - 30 && motionEvent.getRawY() + mResizeStartY + 30 < mTopContainerHeight ) {
                                mCurrentResizeDownTopMargin = (int) motionEvent.getRawY() + mResizeStartY - 30;
                                mCurrentResizeRightTopMargin = mCurrentResizeDownTopMargin / 2 - 15;
                            }
                            break;
                        case "resize_right":
                            if (motionEvent.getRawX() + mResizeStartX > mCurrentWidgetMinWidth - 30 && motionEvent.getRawX() + mResizeStartX + 30 < mTopContainerWidth ) {
                                mCurrentResizeRightLeftMargin = (int) motionEvent.getRawX() + mResizeStartX;
                                mCurrentResizeDownLeftMargin = mCurrentResizeRightLeftMargin / 2 -15;
                            }
                            break;
                    }
                resizeWidget();
                    break;
            }
            return true;
        }
    }

    private void resizeWidget() {
        mCurrentWidgetWidth = mCurrentResizeRightLeftMargin + 30;
        mCurrentWidgetHeight = mCurrentResizeDownTopMargin + 30;
        launcherInfo.hostView.setLayoutParams(new FrameLayout.LayoutParams(mCurrentWidgetWidth, mCurrentWidgetHeight));
        if(mWidgetContainer.getChildCount() != 0) {
            FrameLayout.LayoutParams widgetParams = (FrameLayout.LayoutParams) launcherInfo.hostView.getLayoutParams();
            widgetParams.setMargins(0, 0, 0, mTopContainerHeight - mCurrentWidgetHeight);
            launcherInfo.hostView.setLayoutParams(widgetParams);
        }

        mResizeRightLayoutParams.setMargins(mCurrentResizeRightLeftMargin, mCurrentResizeRightTopMargin,0,0);
        mResizeDownLayoutParams.setMargins(mCurrentResizeDownLeftMargin, mCurrentResizeDownTopMargin, 0, 0);
        mResizeDown.setLayoutParams(mResizeDownLayoutParams);
        mResizeRight.setLayoutParams(mResizeRightLayoutParams);
    }

    private void addAppWidget(Intent data) {
        // TODO: catch bad widget exception when sent
        int appWidgetId = data.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);

        AppWidgetProviderInfo appWidget = mAppWidgetManager.getAppWidgetInfo(appWidgetId);

        if (appWidget.configure != null) {
            // Launch over to configure widget, if needed
            Intent intent = new Intent(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE);
            intent.setComponent(appWidget.configure);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);

            startActivityForResult(intent, REQUEST_CREATE_APPWIDGET);
        } else {
            // Otherwise just add it
            onActivityResult(REQUEST_CREATE_APPWIDGET, Activity.RESULT_OK, data);
        }
    }


    public Context getmContext() {
        return mContext;
    }

    private void loadCarousel() {
//        Log.i("carousel loaded", "true");
        mDockLayout = (LinearLayout) findViewById(R.id.dock_list);
        mPackageManager = getPackageManager();
        List<AppDetail> dockerApps = new ArrayList<>();

        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(width/5, width/5);
        mTopContainer = (RelativeLayout) findViewById(R.id.top_container);
        final RelativeLayout.LayoutParams topContainerParams = new RelativeLayout.LayoutParams(mTopContainer.getLayoutParams());
        mTopContainerWidth = width;
        mTopContainerHeight = height - width/5 - getSoftButtonsBarHeight(this) * 2;
        topContainerParams.height = mTopContainerHeight;
        topContainerParams.topMargin = getSoftButtonsBarHeight(this);
        mTopContainer.setLayoutParams(topContainerParams);

        List<ResolveInfo> availableActivities = mPackageManager.queryIntentActivities(i, 0);
        int numberOfApps = 0;
        for(ResolveInfo ri:availableActivities){
            AppDetail app = new AppDetail();
            app.setmLabel(ri.loadLabel(mPackageManager));
            app.setmName(ri.activityInfo.packageName);
            app.setmIcon(ri.activityInfo.loadIcon(mPackageManager));
            app.setmNumberOfStarts(SharedPrefs.getNumberOfActivityStarts(app.getmLabel().toString(), this));
            if(SharedPrefs.getAppVisible(this, (String) ri.loadLabel(mPackageManager))) {
                if(ri.loadLabel(mPackageManager).toString().equalsIgnoreCase("Settings")) {
                    if(!SharedPrefs.getSafeModeOn(this)) {
                        dockerApps.add(app);
                    }
                } else {
                    dockerApps.add(app);
                }
            }
            numberOfApps++;
        }
        SharedPrefs.setNumberOfApps(numberOfApps, mContext);
        AppsSorter.sortApps(dockerApps, "most used");
        int j;
        for(j = 0; j < dockerApps.size(); j++) {
            View view = LayoutInflater.from(this).inflate(R.layout.dock_item,null);
            TextView homeNotifications = (TextView) view.findViewById(R.id.home_notifications);
            if(dockerApps.get(j).getmLabel().toString().equalsIgnoreCase("Messaging")) {
                SharedPrefs.setMessagingPackageName(this, (String) dockerApps.get(j).getmName());
                view.setTag("Messaging");
                int messageCount = MissedCallsCountRetriever.getUnreadMessagesCount(this);
                if(messageCount > 0) {
                    homeNotifications.setText(String.valueOf(messageCount));
                    homeNotifications.setVisibility(View.VISIBLE);
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) homeNotifications.getLayoutParams();
                    params.rightMargin = width / 36;
                    params.bottomMargin = width / 36;
                } else {
                    homeNotifications.setVisibility(View.GONE);
                }
            } else if(dockerApps.get(j).getmLabel().toString().equalsIgnoreCase("Phone") || dockerApps.get(j).getmLabel().toString().equalsIgnoreCase("Phone")) {
                view.setTag("Phone");
                if(MissedCallsCountRetriever.getMissedCallsCount(this) > 0) {
                    homeNotifications.setText(String.valueOf(MissedCallsCountRetriever.getMissedCallsCount(this)));
                    homeNotifications.setVisibility(View.VISIBLE);

                } else {
                    homeNotifications.setVisibility(View.GONE);
                }
            } else {
                view.setTag(dockerApps.get(j).getmName());
            }

            ImageView iv = (ImageView) view.findViewById(R.id.dock_app_icon);

            final TextView tv = (TextView) view.findViewById(R.id.dock_app_name);
            String path = this.getFilesDir().getAbsolutePath();

            ////////////// load icon from storage ////////////
            //Bitmap icon = IconLoader.loadImageFromStorage(path, (String) dockerApps.get(j).getmLabel());
            Bitmap icon = IconLoader.loadImageFromStorage(path, (String) dockerApps.get(j).getmLabel());
            if(icon == null) {
                icon = ((BitmapDrawable) dockerApps.get(j).getmIcon()).getBitmap();
            }

            iv.setImageDrawable(new BitmapDrawable(this.getResources(), icon));
            iv.setLayoutParams(layoutParams);
            tv.setText(dockerApps.get(j).getmLabel());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent intent;
                        if(v.getTag().toString().equalsIgnoreCase("Phone")) {
                            intent = new Intent(Intent.ACTION_DIAL);
                        } else {
                            if(v.getTag().toString().equals("Messaging")) {
                                intent = mPackageManager.getLaunchIntentForPackage(SharedPrefs.getMessagingPackageName(HomeActivity.this));
                            } else {
                                intent = mPackageManager.getLaunchIntentForPackage(v.getTag().toString());
                            }
                        }
                        SharedPrefs.increaseNumberOfActivityStarts(((TextView)v.findViewById(R.id.dock_app_name)).getText().toString(), mContext);
                        SharedPrefs.setHomeReloadRequired(true, HomeActivity.this);

                        if(intent != null) {
                            mContext.startActivity(intent);
                        } else {
                            mContext.startActivity(new Intent(v.getTag().toString()));
                        }
                    } catch (Exception e) {
                        Toast.makeText(HomeActivity.this,"This application cannot be opened" ,
                                Toast.LENGTH_SHORT).show();
                        recreate();
                    }
                }
            });
            mDockLayout.addView(view);

        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mGestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onDown(MotionEvent event) {
            return true;
        }

    }


    private boolean sameNoOfAllApps() {
        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> availableActivities = mPackageManager.queryIntentActivities(i, 0);
        int numberOfApps = 0;
        for(ResolveInfo ignored :availableActivities){
            numberOfApps++;
        }
        return SharedPrefs.getNumberOfApps(mContext) == numberOfApps;
    }

    @Override
    protected void onResume() {
        mDockLayout = (LinearLayout) findViewById(R.id.dock_list);
        int dockCount = mDockLayout.getChildCount();
        if(dockCount > 0) {
            ((HorizontalScrollView) mDockLayout.getParent()).scrollTo(0,0);
        }
        if(SharedPrefs.getHomeReloadRequired(this) || (SharedPrefs.getVisibleCount(this) > 0 && mDockLayout.getChildCount() != SharedPrefs.getVisibleCount(this)) || !sameNoOfAllApps()) {
            SharedPrefs.setHomeReloadRequired(false, this);
            SharedPrefs.setVisibleCount(mDockLayout.getChildCount(), this);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mDockLayout.removeAllViews();
                    loadCarousel();
                }
            }, 1);
        }
        if (SharedPrefs.getHomeRecreateRequired(mContext)){
            SharedPrefs.setHomeRecreateRequired(false, mContext);
            if(mManager!= null && mView != null) {
                mManager.removeView(mView);
            }


        }

        toggleNavigationBar();
        super.onResume();
    }

    @Override
    protected void onPause() {

        if(mSmsReceiver != null) {
            unregisterReceiver(mSmsReceiver);
            mSmsReceiver = null;
        }
        if(missedCallObserver != null) {
            getApplicationContext().getContentResolver().unregisterContentObserver(missedCallObserver);
        }
        if(mPackageUpdateReceiver != null) {
            unregisterReceiver(mPackageUpdateReceiver);
            mPackageUpdateReceiver = null;
        }
        if(mWidgetContainer.getChildCount() > 0) {
            for(int i = 0; i < (mWidgetContainer.getChildCount()); i++) {
                ((WidgetFrame) mWidgetContainer.getChildAt(i)).getAppWidgetHost().stopListening();
            }
        }
        super.onPause();

    }

    @Override
    protected void onStop() {

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
